# Ubuntu-commands

MobaXterm X server  
> echo $DISPLAY  
> export DISPLAY="127.0.0.1:11.0" or 12.0, etc.

file send  
> rsync -avzh --progress  -e 'ssh -p 1234' id@host:/server_path   local_des_path --info=progress2  
> rsync -avzh from_path to_path  --info=progress2  
> scp -r -P port user@ip:/A /B  

To undo git add before a commit  
> git reset <file> or git reset to unstage all changes.  

Ignore local changes and git pull  
> git reset --hard  
or  
> git stash push --include-untracked  
then  
> git pull  

How to clone git repository with specific revision/changeset?  
> git checkout tags/v1
> git status

How to kill a range of consecutive processes in Linux?
`kill -9 {3457..3464}`  
`killall python` 

remove multi file by sub string  
`find -type f -name '*substring*' -delete`

Replace content in multiple txt  
> find . -type f -name '*.txt' -exec sed -i 's/\t/,/g' {} +  

> find . -type f -name "*.py" -exec sed -i 's/AAA/BBB/g' {} +  

Remove content in multiple txt    
> find . -name "*_time.txt" -type f -exec rm {} +  

add_prefix_to_txtfilename  
> for file in *; do mv -v ${file} nfs_${file}; done  

rename txt files  
```
for f in *005*; do
  mv "$f" "${f//005/001}"
done
```


add_posefix_to_txtfilename  

> for file in *.py; do mv "$file" "${file%.py}_CT05.py"; done  

disk speed test  
> sudo hdparm -tv /dev/sda3

sort disk ssd or hdd
> sudo lsblk

show disk model
> sudo lsblk --scsi

Graphic service stop
`sudo service lightdm stop`
`sudo service gdm stop`
or  
To stop:  sudo init 3  
To resume:  sudo init 5  

recursively delete all files of a specific extension in the current directory
`find . -name "*.PNG" -type f -delete`

install virtualenv `apt-get install python3.6-dev python3.6-venv`

create virtualenv `python3.6 -m venv env_name`

conda create --name env_name python=n.k


Options for Deleting Lines in Vim:

> `Esc`
> 
> Delete the following 3 lines: `3dd`
> 
> Delete All Lines :`:%d`

Save current working directory `cwd=$(pwd)`

TP-Link AC600 Archer T2U Nano driver for Ubuntu 18.04
```
sudo apt install git dkms
git clone https://github.com/jeremyb31/rtl8812au-1.git
cd rtl8812au-1
sudo ./dkms-install.sh
```

Screen
> apt-get install screen  
> screen -S sessionname  
> return back : CTRL + A, followed by D  
> resume to session : screen -r sessionname  
> resume to session : : screen -d sessionname  
> show : screen -ls  
> screen -XS session-id quit  


Real-time insight on used resources
> watch -n0.1 nvidia-smi

Soft link
> ln -s /A /B

CUDA options
> CUDA_VISIBLE_DEVICES=1,..,X

determine the total size of a directory (folder)
> du -hs /

Memory check and release
> free -m  
> echo 3 > /proc/sys/vm/drop_caches && swapoff -a && swapon -a && printf '\n%s\n' 'Ram-cache and Swap Cleared'

write log  
> cmd  2>&1 | x.log

CPU usage  
> sar -u 1

unzip files  
> for z in zip/*.zip; do yes A | unzip "$z" -d data; done  
> tar xvf A.tar  

zip multiple directories into individual zip files  
> for i in */; do zip -0 -r "${i%/}.zip" "$i" & done; wait

zip multi files with name BBB* as AAA.zip
> zip -r AAA.zip BBB*/

pip package
> pip show package
> pip list
> pip uninstall

count folder
> ls -l  | wc -l

shell remove \r 
> tr -d '\r' < infile > outfile  

give folder permissions  
> sudo chown -R testuser:testuser /folder

# Windows-command
> tar -xf archive.zip
